function calculateChange(input, output) {
  const generate = Math.floor(arguments);
  const validate = [
    100000, 50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100,
  ];

  if (input < output) {
    return "your money is not enough";
  }

  return input - output;
}

console.log(calculateChange(700649, 800000));
console.log(calculateChange(575650, 580000));
console.log(calculateChange(657650, 600000));
