janken = (input) => {
  const generate = Math.floor(Math.random() * 2);
  let give;

  const validate = ["rock", "scissor", "papper"];

  if (!validate.includes(input)) {
    return "wrong input";
  }

  if (generate === 0) {
    give = "rock";
  }

  if (generate === 1) {
    give = "scissor";
  }

  if (generate === 2) {
    give = "papper";
  }

  console.log("================");
  if (input === "rock" && give === "rock") {
    return "Draw";
  }
  if (input === "rock" && give === "scissor") {
    return "You win";
  }
  if (input === "rock" && give === "papper") {
    return "You lost";
  }

  if (input === "scissor" && give === "rock") {
    return "You lost";
  }
  if (input === "scissor" && give === "scissor") {
    return "Draw";
  }
  if (input === "scissor" && give === "papper") {
    return "You win";
  }

  if (input === "papper" && give === "rock") {
    return "You win";
  }
  if (input === "papper" && give === "scissor") {
    return "You lost";
  }
  if (input === "papper" && give === "papper") {
    return "Draw";
  }
};

console.log(janken("rock"));
console.log(janken("papper"));
console.log(janken("scissor"));
console.log(janken("nuclear"));
console.log(janken(1));
